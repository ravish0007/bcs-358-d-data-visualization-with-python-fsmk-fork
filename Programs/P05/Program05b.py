import matplotlib.pyplot as plt

#Number of FIFA World Cup wins for different countries
countries = ['Brazil', 'Germany', 'Italy', 'Argentina', 'Uruguay', 'France', 'England', 'Spain']
wins = [5, 4, 4, 3, 2, 2, 1, 1]  # Replace with actual data

# Colors for each country
colors = ['yellow', 'magenta', 'green', 'blue', 'lightblue', 'blue', 'red', 'cyan']

def make_autopct(values):
    def my_autopct(pct):
        total = sum(values)
        val = int(round(pct*total/100.0))
        return '{v:d}'.format(v=val)
    return my_autopct
    
    
# Create a pie chart
plt.pie(wins, labels=countries, autopct=make_autopct(wins), colors=colors, startangle=90, explode=[0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2], shadow=True)

#Uncomment next line and comment previous line if you want to show in percentages
#plt.pie(wins, labels=countries, autopct='%1.1f%%', colors=colors, startangle=90, explode=[0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2], shadow=True)

# Add title
plt.title('FIFA World Cup Wins by Country')

# Display the plot
plt.axis('equal')  # Equal aspect ratio ensures that the pie chart is circular.
plt.show()


