import plotly.graph_objects as go
import numpy as np

# Generate sample 3D data
x = np.linspace(-5, 5, 100)
y = np.linspace(-5, 5, 100)
x, y = np.meshgrid(x, y)
z = np.sin(np.sqrt(x**2 + y**2))

# Create a 3D surface plot
fig = go.Figure(data=[go.Surface(z=z, x=x, y=y)])

# Customize layout
fig.update_layout(scene=dict(
                    xaxis_title='X Axis',
                    yaxis_title='Y Axis',
                    zaxis_title='Z Axis'),
                margin=dict(l=0, r=0, b=0, t=40),
                title='3D Surface Plot of sin(sqrt(x^2 + y^2))')

# Display the 3D surface plot
fig.show()
